<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin'], function () {

    //GET
    Route::get('/', 'AuthController@auth')->name('admin_auth');
    Route::get('/logout', 'AuthController@logout')->name('admin_logout');

    Route::get('/dashboard', 'AdminPagesController@dashboard')->name('admin_dashboard');
    Route::get('/users', 'AdminPagesController@users')->name('admin_users');

    //POST
    Route::post('/login_attemp', 'AuthController@login_attemp');

    Route::get('/ajax_users', 'AdminAjaxUsersController@ajax_users')->name('admin_ajax_users');
    Route::get('/wallet_users/{id}', 'AdminAjaxUsersController@wallet_users')->name('admin_wallet_users');

});

Auth::routes();

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/activate-user/{email}/{token}', 'ActivateController@activate')->name('user_activate');
    Route::get('/activate-user-change-email/{email}/{email_old}/{token}', 'ActivateController@activate_change_email')->name('user_activate_change_email');
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@index')->name('empty');
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::post('/change_password', 'HomeController@change_password')->name('change_password');
    Route::post('/change_email', 'HomeController@change_email')->name('change_email');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/ethereum-wallet', 'HomeController@ethereum_wallet')->name('ethereum_wallet');
    Route::get('/buy-cmd', 'HomeController@buy_cmd')->name('buy_cmd');
    Route::post('/save-ethereum-wallet', 'HomeController@save_ethereum_wallet')->name('save_ethereum_wallet');
    Route::post('/save-ethereum-wallet_ajax', 'HomeController@save_ethereum_wallet_ajax')->name('save_ethereum_wallet_ajax');
    Route::post('/generate-temporary-wallet', 'HomeController@generate_temporary_wallet')->name('generate_temporary_wallet');
    Route::post('/generate-temporary-wallet', 'HomeController@generate_temporary_wallet')->name('generate_temporary_wallet');
    Route::any('/get-token-rate', 'HomeController@get_token_rate')->name('get_token_rate');
    Route::post('/get-transaction-status', 'HomeController@detect_transaction')->name('detect_transaction');
});

