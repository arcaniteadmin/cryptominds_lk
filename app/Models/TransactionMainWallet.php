<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'transaction_main_wallet';

    protected $fillable = [
        'user_id',
        'coin',
        'wallet_id',
        'wallet_address',
        'main_wallet_address',
        'passphrase',
        'json_data',
    ];
}
