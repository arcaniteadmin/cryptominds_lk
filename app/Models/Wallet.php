<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallets';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'coin',
        'wallet_id',
        'wallet_address',
        'passphrase',
        'created_at',
    ];
}
