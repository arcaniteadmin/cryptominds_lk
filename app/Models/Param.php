<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
    protected $table = 'params';
    public $timestamps = false;

    protected $fillable = [
        'key',
        'value',
    ];

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function setParam($key, $value)
    {
        $check_key = Param::where('key', $key)->first();
        if (isset($check_key)) {
            $check_key->value = $value;
            $check_key->save();
        } else {
            $new_key = new Param();
            $new_key->key = $key;
            $new_key->value = $value;
            $new_key->save();
        }

        return true;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getParam($key)
    {
        $check_key = Param::where('key', $key)->first();
        if (isset($check_key)) {
            return $check_key->value;
        }

        return NULL;
    }
}
