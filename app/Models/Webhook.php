<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    protected $table = 'webhooks';

    protected $fillable = [
        'user_id',
        'webhook_id',
        'wallet_id',
        'coin',
        'type',
        'url',
    ];
}
