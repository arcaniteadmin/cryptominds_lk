<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'wallet_id',
        'coin',
        'amount',
        'rate',
        'eth_amount',
        'token_count',
        'json_data',
    ];
}
