<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminPagesController extends Controller
{
    /**
     * AdminPagesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['admin_auth_redirect']);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard() {
        return view('admin/pages/dashboard');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function users() {
        return view('admin/pages/users');
    }
}
