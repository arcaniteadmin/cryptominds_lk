<?php

namespace App\Http\Controllers\Admin;

use App\Models\Param;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Wallet;
use App\Models\Transaction;

class AdminAjaxUsersController extends Controller
{
    public function ajax_users(Request $request) {
        $search = $request->input('search', false);

        $count_per_page = $request->input('length', 10);
        $offset = $request->input('start', 0);
        $order = $request->input('order', [[ 'column'=>'0', 'dir' => 'asc' ]]);
        $field_map = ['id', 'login', 'versionApp', 'platform', 'created_at', 'updated_at', 'coin'];


        if ($search && isset($search['value']) && !empty($search['value'])) {

            $users = User::where(function ($query) use ($search) {
                $query->where('id', 'like', $search['value'].'%')
                    ->orWhere('name', 'like', $search['value'].'%')
                    ->orWhere('email', 'like', $search['value'].'%')
                    ->orWhere('eth_wallet', 'like', $search['value'].'%');
            })
                ->offset($offset)
                ->limit($count_per_page)
                ->orderBy($field_map[$order[0]['column']], $order[0]['dir'])
                ->get();
            $total_count = $users->count();
        } else {
            $users = User::offset($offset)->limit($count_per_page)->orderBy($field_map[$order[0]['column']], $order[0]['dir'])->get();
            $total_count = User::all()->count();
        }
        return response()->json(["drow" => 1,"data" => $users, "recordsFiltered" => $total_count,"recordsTotal" => $total_count]);
    }

    public function wallet_users(Request $request, $id) {

        $search = $request->input('search', false);

        $count_per_page = $request->input('length', 10);
        $offset = $request->input('start', 0);
        $order = $request->input('order', [[ 'column'=>'0', 'dir' => 'asc' ]]);
        $field_map = ['id', 'coin'];

        if ($search && isset($search['value']) && !empty($search['value'])) {
            $transactions = Transaction::where('user_id', $id)->where(function ($query) use ($search) {
                $query->where('id', 'like', $search['value'].'%')
                    ->orWhere('wallet_id', 'like', $search['value'].'%')
                    ->orWhere('coin', 'like', $search['value'].'%')
                    ->orWhere('amount', 'like', $search['value'].'%')
                    ->orWhere('rate', 'like', $search['value'].'%')
                    ->orWhere('eth_amount', 'like', $search['value'].'%')
                    ->orWhere('token_count', 'like', $search['value'].'%');
            })
                ->offset($offset)
                ->limit($count_per_page)
                ->orderBy($field_map[$order[0]['column']], $order[0]['dir'])
                ->get();
            $total_count = $transactions->count();
        } else {
            $transactions = Transaction::where('user_id', $id)->offset($offset)->limit($count_per_page)->orderBy($field_map[$order[0]['column']], $order[0]['dir'])->get();
            $total_count = Transaction::where('user_id', $id)->count();
        }

        return response()->json(["drow" => 1,"data" => $transactions, "recordsFiltered" => $total_count,"recordsTotal" => $total_count]);
    }
}
