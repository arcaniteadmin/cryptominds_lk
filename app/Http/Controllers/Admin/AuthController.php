<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function auth() {
        return view('admin/auth/login');
    }

    public function logout() {
        Auth::logout();
        return Redirect::route('admin_auth');
    }

    public function login_attemp(Request $request) {
        $login = $request->validation['login'];
        $password = $request->validation['password'];

        if (isset($login) && isset($password)) {
            $check_user = User::where('email', '=', $login)->first();
            if (isset($check_user->id)) {

                if ($check_user->is_admin == 1) {
                    $credentials = ['email' => $login, 'password' => $password];
                    if (Auth::attempt($credentials, true)) {
                        return Redirect::route('admin_dashboard');
                    } else {
                        Session::flash('auth_error', 'При авторизации возникла ошибка, обратитесь к администрации');
                        return Redirect::route('admin_auth');
                    }
                } else {
                    Session::flash('auth_error', 'Пользователь не является администратором');
                    return Redirect::route('admin_auth');
                }

            } else {
                Session::flash('auth_error', 'Пользователя с таким логином не существует');
                return Redirect::route('admin_auth');
            }
        } else {
            Session::flash('auth_error', 'Введите логин и пароль');
            return Redirect::route('admin_auth');
        }
    }
}
