<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ResetLink;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.

        $email = $request->email;
        if($user = User::where('email', $request->input('email') )->first())
        {
            $check_token = DB::table('password_resets')
                ->where('email', $email)
                ->first();
            if (!$check_token) {
                $token = str_random(64);
                DB::table('password_resets')->insert([
                    'email' => $user->email,
                    'token' => $token,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            } else {
                $token = $check_token->token;
            }

            Mail::to($email)->send(new ResetLink($token));

            return redirect()->back()->with('status', trans(Password::RESET_LINK_SENT));
        }

        return redirect()->back()->withErrors(['email' => trans(Password::INVALID_USER)]);
    }
}
