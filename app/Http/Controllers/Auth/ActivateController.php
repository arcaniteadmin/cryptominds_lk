<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserActivation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\Auth;

class ActivateController extends Controller
{
    public function activate($email, $token)
    {
        $check_email = UserActivation::where('email', $email)->first();
        if ($check_email) {
            if ($check_email->token == $token) {
                $user = User::where('email', $email)->first();
                if (isset($user)) {
                    $user->is_active = 1;
                    $user->save();
                    Session::flash('success_text', "Email confirmed!");
                }
            }
        }

        return redirect('/login');
    }

    public function activate_change_email($email,$email_old, $token)
    {
        $check_email = UserActivation::where('email', $email)->first();

        if ($check_email) {
            if ($check_email->token == $token) {
                $user = User::where('email', $email_old)->first();
                if (isset($user)) {
                    $user->email = $email;
                    $user->save();
                    Session::flash('success_text', "Email confirmed!");
                }
            }
        }
        return redirect('/login');
    }

}
