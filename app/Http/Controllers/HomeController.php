<?php

namespace App\Http\Controllers;

use App\Core\BitgoCore;
use App\Http\Requests\PasswordRequest;
use App\Models\Param;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\Webhook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\View\View;
use App\Models\UserActivation;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
    protected $bitgoCore;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->bitgoCore = new BitgoCore(true);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user_id = Auth::id();
        $user_data = User::where('id', $user_id)->first();
        $user_token_count = $this->_getUserTokenCount();
        $user_transactions = Transaction::where('user_id', $user_id)->get();
        return view('home', [
            'user_data' => $user_data,
            'countTokens' => $user_token_count,
            'transactions' => $user_transactions,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ethereum_wallet()
    {
        $user = Auth::user();
        $user_token_count = $this->_getUserTokenCount();
        return view('ethereum_wallet', [
            'user' => $user,
            'countTokens' => $user_token_count,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function profile() {
        $user = Auth::user();
        $user_token_count = $this->_getUserTokenCount();
        return view('pages.profile',['countTokens' => $user_token_count])->withUser($user);
    }

    public function change_password(Request $request) {
        $password = $request->input('password', false);
        $user = Auth::user();
        $user->password = bcrypt($password);
        $user->save();
        Session::flash('success_password', 'Your password was successfully updated');
        return redirect('/profile')->withUser($user);
    }

    public function change_email(Request $request) {
        $email_old = Auth::user()->email;
        $token = rand(5,100);
        $email = $request->email['email'];
        $user = Auth::user();
        $check_email = UserActivation::where('email', $email)->first();
        if ($check_email) {
            $check_email->email = $email;
            $check_email->token = $token;
            $check_email->created_at = date('Y-m-d H:i:s');
            $check_email->save();
        } else {
            $newActivationRecord = new UserActivation();
            $newActivationRecord->fill([
                'email' => $email,
                'token' => $token,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            $newActivationRecord->save();
        }

        // Send email
        Mail::to($email)->send(new \App\Mail\ChangeEmailActivation($email, $token, $email_old));

        Session::flash('success_email', 'You need to activate link that was sent to '.$email);
        return redirect('/profile')->withUser($user);
        }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save_ethereum_wallet(Request $request)
    {
        $address = $request->input('address', false);
        $user = Auth::user();
        if ($this->bitgoCore->isAddress($address)) {
            //Save eth wallet
            $user->eth_wallet = $address;
            $user->save();

            Session::flash('status', 'Your ethereum wallet saved!');
            return redirect(route('home'));
        } else{
            Session::flash('eth_wallet_err', 'Wrong ethereum address');
            return redirect(route('ethereum_wallet'));
        }
    }

    public function save_ethereum_wallet_ajax(Request $request)
    {
        $address = $request->input('address', false);
        $user = Auth::user();
        if ($this->bitgoCore->isAddress($address)) {
            //Save eth wallet
            $user->eth_wallet = $address;
            $user->save();
            return response()->json(['status' => true]);
        } else if ($address == "") {
            return response()->json(['adress' => 'NULL']);
        } else {
            return response()->json(['status' => false]);
        }
    }

    /**
     * Сгенерировать временный кошелек
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generate_temporary_wallet(Request $request)
    {
        $status = false;
        $message = "";
        $data = [];
        $currency = $request->input('currency', false);

        if ($currency) {
            // Проверяем, ввел ли пользователь эфир кошелек
            if (isset(Auth::user()->eth_wallet)) {

                if ($currency == 'eth' || $currency == 'teth') {
                    $status = true;
                    $message = 'success';
                    $data['wallet'] = $_ENV['eht_ico_address'];
                } else {
                    // Проверяем, есть ли у пользователя кошелек в БД
                    $check_wallet = Wallet::where('user_id', Auth::user()->id)->where('coin', $currency)->first();

                    if (isset($check_wallet->id)) {

                        $data['wallet'] = $check_wallet->wallet_address;

                    } else {
                        $passphrase = $this->bitgoCore->generateRandomString(15);
                        $wallet = $this->bitgoCore->generateWallet(Auth::user()->id, $currency, $passphrase);
                        $trigger_url = isset($_ENV['bitgo_trigger_url']) ? $_ENV['bitgo_trigger_url'] : false;

                        if ($trigger_url && $wallet) {
                            if (isset($wallet->id) && isset($wallet->receiveAddress->address)) {
                                $confirm_code = $this->bitgoCore->generateRandomString(30);
                                $newWallet = new Wallet();
                                $newWallet->fill([
                                    'user_id' => Auth::user()->id,
                                    'coin' => $currency,
                                    'wallet_id' => $wallet->id,
                                    'wallet_address' => $wallet->receiveAddress->address,
                                    'passphrase' => $passphrase,
                                    'created_at' => date('Y-m-d H:i:s'),
                                ]);
                                $newWallet->save();
                                $data['wallet'] = $wallet->receiveAddress->address;

                                //Create transfer webhook
                                $transfer_webhook = $this->bitgoCore->addWebhook($currency, $wallet->id, $trigger_url, 'transfer', $confirm_code);
                                if ($transfer_webhook && isset($transfer_webhook->id)) {
                                    $newTransferWebhook = new Webhook();
                                    $newTransferWebhook->fill([
                                        'user_id' => Auth::user()->id,
                                        'webhook_id' => $transfer_webhook->id,
                                        'wallet_id' => $transfer_webhook->walletId,
                                        'coin' => $transfer_webhook->coin,
                                        'type' => $transfer_webhook->type,
                                        'url' => $transfer_webhook->url,
                                    ]);
                                    $newTransferWebhook->save();
                                }
                            } else {
                                $data['error'] = "true";
                                $data['message'] = "This coin temporary not available. Please try later.";
                            }
                        } else {
                            $data['error'] = "true";
                            $data['message'] = "This coin temporary not available. Please try later.";
                        }
                    }

                    $status = true;
                    $message = 'success';
                }
            } else {
                $status = false;
                $message = "missing_eth_address";
            }

        } else {
            $message = 'missing currency parameter';
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detect_transaction(Request $request)
    {
        $status = false;
        $data = [];

        $address = $request->input('address', false);
        $start_time = $request->input('datetime', false);
        $timezone = $request->input('time_zone', false);

        if ($address && $timezone && $start_time && $this->validateDate($start_time)) {
            $wallet = Wallet::where('wallet_address', $address)->first();
            $user_datetime = new \DateTime($start_time, new \DateTimeZone($timezone));
            $moscow_tz = new \DateTimeZone('Europe/Moscow');
            $user_datetime->setTimezone($moscow_tz);
            $finally_datetime = $user_datetime->format('Y-m-d H:i:s');

            if (isset($wallet->id)) {
                $transaction = Transaction::where('user_id', Auth::user()->id)->where('wallet_id', $wallet->wallet_id)->where('created_at', '>', $finally_datetime)->first();
                if (isset($transaction->id)) {
                    $status = true;
                    $data = [
                        'crypto_amount' => $transaction->amount . ' ' . $transaction->coin,
                        'cmd_count' => $transaction->token_count,
                    ];
                }
            }
        }

        return response()->json([
            'status' => $status,
            'data' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buy_cmd(Request $request)
    {
        $user_token_count = $this->_getUserTokenCount();
        return view('buy', [
            'countTokens' => $user_token_count,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_token_rate(Request $request)
    {
        $param = new Param();
        return response()->json([
            'rate' => str_replace(",", ".", $param->getParam('token_rate')),
        ]);
    }

    /**
     * @return mixed
     */
    protected function _getUserTokenCount()
    {
        return Transaction::where('user_id', Auth::user()->id)->sum('token_count');
    }

    /**
     * @param $date
     * @param string $format
     * @return bool
     */
    protected function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
