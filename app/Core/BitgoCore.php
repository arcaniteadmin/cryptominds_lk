<?php

namespace App\Core;

Class BitgoCore {

    private $__accessToken; // bitgo access токен, устанавливается в .env
    public $API_Endpoint;
    const BITGO_PRODUCTION_API_ENDPOINT = 'https://www.bitgo.com/api/v2';
    const BITGO_TESTNET_API_ENDPOINT = 'https://test.bitgo.com/api/v2';
    public $Rest_Api_Express_Post;

    public function __construct()
    {
        $this->__accessToken = isset($_ENV['bitgo_token']) ? $_ENV['bitgo_token'] : false;
        $this->Rest_Api_Express_Post = isset($_ENV['bitgo_express_port']) ? $_ENV['bitgo_express_port'] : 3080;

        $this->API_Endpoint = "http://localhost:".$this->Rest_Api_Express_Post."/api/v2";
    }

    /**
     * Проверяет ethereum адрес кошелька на валидность
     * @param $address
     * @return bool
     */
    public function isAddress($address)
    {
        if (!preg_match('/^(0x)?[0-9a-f]{40}$/i',$address)) {
            // check if it has the basic requirements of an address
            return false;
        } elseif (!preg_match('/^(0x)?[0-9a-f]{40}$/',$address) || preg_match('/^(0x)?[0-9A-F]{40}$/',$address)) {
            // If it's all small caps or all all caps, return true
            return true;
        }

        return false;
    }

    /**
     * Сгенерировать кошелек в bitgo
     * @param $coin
     * @return mixed
     */
    public function generateWallet($user_id, $coin, $passhprase)
    {
        $url = $this->API_Endpoint . "/" . $coin . "/wallet/generate";
        $params = [
            "label" => $coin . " wallet for uid " . $user_id . " hash " . $this->generateRandomString(5),
            "passphrase" => $passhprase,
        ];

        return $this->__execute('POST', $url, $params);
    }

    /**
     * Добавить вебхук к кошельку
     * @param $coin
     * @param $wallet_id
     * @param $trigger_url
     * @param $confirm_code
     * @return mixed
     */
    public function addWebhook($coin, $wallet_id, $trigger_url, $type, $confirm_code)
    {
        $url = $this->API_Endpoint . "/" . $coin . "/wallet/". $wallet_id ."/webhooks";
        $params = [
            'url' => $trigger_url,
            'type' => $type,
            'numConfirmations' => 0,
        ];

        return $this->__execute('POST', $url, $params);
    }

    /**
     * Выполнить curl запрос к API BitGO
     * @param string $requestType
     * @param $url
     * @param array $params
     * @return mixed
     */
    private function __execute($requestType = 'POST', $url, $params = [])
    {
        $ch = curl_init($url);
        $data_string = json_encode($params);
        if ($requestType === 'GET') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        } elseif ($requestType === 'PUT') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        } elseif ($requestType === 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        } elseif ($requestType === 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'Authorization: Bearer ' . $this->__accessToken
        ));
        //curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    /**
     * Сгенерировать рандомную строку
     * @param int $length
     * @return string
     */
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}