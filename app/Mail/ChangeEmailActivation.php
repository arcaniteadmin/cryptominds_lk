<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeEmailActivation extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $token;
    public $email_old;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $token, $email_old)
    {
        $this->email = $email;
        $this->token = $token;
        $this->email_old = $email_old;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@cryptominds.me')
            ->view('emails.change_email_activation')
            ->with([
                'email' => $this->email,
                'token' => $this->token,
                'email_old' => $this->email_old,
            ]);
    }
}
