<?php

namespace App\Console\Commands;

use App\Models\Param;
use Illuminate\Console\Command;

class setTokenRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set_token_rate {rate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rate = $this->argument('rate');
        if ($rate) {

            $param = new Param();
            if ($param->setParam('token_rate', $rate)) {
                echo "token rate changed success. \n";
            } else {
                echo "Error at change token rate. \n";
            }

        } else {
            echo "Missing rate \n";
        }
    }
}
