/* NodeJS скрипт для обработки запросов с BitGo (оракул скрипт) */

var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var request = require('request');
var Web3 = require('web3');
var Tx = require('ethereumjs-tx');
require('dotenv').config(); // Библиотека для парсинга из .env файла ларавеля

// Подключаем BitGo
var BitGo = require('bitgo');
var bitgo = new BitGo.BitGo({ env: process.env.bitgo_env, accessToken: process.env.bitgo_token});

var app = express();

var db_config = {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
};

var con; // Переменная для соединения с mysql

// Разворачиваем сервер на порту
app.listen(process.env.PORT, function() {
    console.log('server listening on: ' + process.env.PORT);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(bodyParser.text());

// тест функции отправки токенов
//sendTokens(300000000, "0x3718df6eE066220204a0538FB44e408c3218D3c5");

// Обработчик POST запроса от bitgo
app.post('/bitgo-callback', function(req, res){
    var webhookData = req.body;

    if (typeof webhookData == 'string') {
        webhookData = JSON.parse(webhookData);
    }

    //Проверяем на валидность полей
    if (webhookData.hash && webhookData.transfer && webhookData.coin && webhookData.type && webhookData.wallet) {
        // Сохраняем триггер BitGo в таблицу
        var sql = "INSERT INTO bitgo_callback (json_data, created_at) VALUES ('"+JSON.stringify(webhookData)+"', '"+getDateTime()+"')";
        con.query(sql, function (err, result) {
            if (err) throw err;
        });

        // Получаем текущий курс обмена и дальнейшую логику передаем в callback
        getExchangeRate(webhookData.coin, function (rate) {

            // Получаем кошелек из BitgoSDK
            bitgo.coin(webhookData.coin).wallets().get({ id: webhookData.wallet })
                .then(function(wallet) {
                    if (wallet) {
                        //  Получаем инфу о транзакции через BitgoSDK
                        wallet.getTransaction({txHash: webhookData.hash})
                            .then(function(transaction){
                                if (transaction) {
                                    if (transaction.confirmations == 0 && transaction.outputs && transaction.outputs[0]) {
                                        // Item транзакции который мы найдем, в коротом будет сумма перевода
                                        var transaction_finded_item = undefined;

                                        // Проходимся по всем item'мам транзакции, чтобы найти item с суммой перевода
                                        var output_item = {};
                                        for (var output_item_num in transaction.outputs) {
                                            output_item = transaction.outputs[output_item_num];
                                            // Проверяем что это нужный нам объект в транзакции
                                            if (output_item.wallet != undefined && output_item.wallet == webhookData.wallet) {
                                                transaction_finded_item = output_item;
                                                break;
                                            }
                                        }

                                        if (transaction_finded_item) {
                                            var correct_amount = 0;
                                            switch (webhookData.coin) {
                                                case 'btc':
                                                    correct_amount = transaction_finded_item.value / 100000000; // convert satoshi to bitcoin
                                                    break;
                                                case 'tbtc':
                                                    correct_amount = transaction_finded_item.value / 100000000; // convert satoshi to bitcoin
                                                    break;
                                                case 'tbch':
                                                    correct_amount = transaction_finded_item.value / 100000000;
                                                    break;
                                                case 'bch':
                                                    correct_amount = transaction_finded_item.value / 100000000;
                                                    break;
                                                case 'ltc':
                                                    correct_amount = transaction_finded_item.value / 100000000;
                                                    break;
                                                case 'tltc':
                                                    correct_amount = transaction_finded_item.value / 100000000;
                                                    break;
                                                case 'eth':
                                                    correct_amount = transaction_finded_item.value / 100000000;
                                                    break;
                                                case 'teth':
                                                    correct_amount = transaction_finded_item.value / 100000000;
                                                    break;
                                            }

                                            var converted_eth_amount = correct_amount * rate;
                                            var token_count = converted_eth_amount / getCMDprice();

                                            var sql_get_user = "SELECT user_id, passphrase FROM `wallets` WHERE `coin`='" + webhookData.coin + "' AND `wallet_id`='" + webhookData.wallet + "'";
                                            con.query(sql_get_user, function (err, result) {
                                                if (err) throw err;
                                                if (result && result[0]) {
                                                    var user_id = result[0].user_id;
                                                    var passphrase = result[0].passphrase;
                                                    var sql_insert_transaction = "INSERT INTO transactions (user_id, wallet_id, coin, amount, rate, eth_amount, token_count, json_data, created_at, updated_at) VALUES " +
                                                        "('" + user_id + "', '" + webhookData.wallet + "', '" + webhookData.coin + "', '" + correct_amount + "', '" + rate + "', '" + converted_eth_amount + "', '" + token_count + "', '" + JSON.stringify(transaction) + "', '" + getDateTime() + "', '" + getDateTime() + "')";
                                                    con.query(sql_insert_transaction, function (err, result) {
                                                        if (err) throw err;
                                                        console.log('Transaction saved success');

                                                        // Узнаем эфир кошелек пользователя
                                                        var sql_get_user_eth_wallet = "SELECT eth_wallet FROM users WHERE id='"+user_id+"'";
                                                        con.query(sql_get_user_eth_wallet, function (err, result_eth_wallet) {
                                                            if (err) throw err;
                                                            if (result_eth_wallet[0] && result_eth_wallet[0].eth_wallet) {
                                                                var eth_wallet = result_eth_wallet[0].eth_wallet;
                                                                // Вызываем функцию отправки токенов
                                                                sendTokens(token_count * 1e8, eth_wallet);
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                    }
                });
        });
    }



    // В любом случае отвечаем ok, будем вводить хакеров в заблуждение :)
    res.json({ok:true});
});

function sendTokens(count, eth_wallet) {
    if (parseInt(count) && parseInt(count) > 0 && eth_wallet) {
        var partPrivKey = "1472f5dd0e555ba91a2790fa48a85db790c33c9be5d941559204155bd5325fc9";
        // Вызываем функцию смарт контракта для отправки токенов
        var web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/kuQ2Z2XSHLEhRPn3oKXb"));
        // Private key аккаунта с которого отправляем
        var account = web3.eth.accounts.privateKeyToAccount('0x' + partPrivKey);
        // Интерфейс контракта ICO
        var contractInterface = [ { "constant": false, "inputs": [ { "name": "_countTokens", "type": "uint256" }, { "name": "_ethWallet", "type": "address" } ], "name": "sendTokens", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_countTokens", "type": "uint256" }, { "name": "_ethWallet", "type": "address" } ], "name": "sendOracleTokens", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "buyPrice", "outputs": [ { "name": "", "type": "uint256", "value": "1000000" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "buy", "outputs": [ { "name": "", "type": "uint256" } ], "payable": true, "stateMutability": "payable", "type": "function" }, { "constant": true, "inputs": [], "name": "token", "outputs": [ { "name": "", "type": "address", "value": "0xc49eaf63e0ba45e54b4cd400d904aa52052df452" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [ { "name": "_token", "type": "address", "index": 0, "typeShort": "address", "bits": "", "displayName": "&thinsp;<span class=\"punctuation\">_</span>&thinsp;token", "template": "elements_input_address", "value": "0xc49eaF63e0bA45E54b4CD400d904Aa52052DF452" } ], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" } ];
        // Адресс смарт контракта ICO
        var contractAddress = "0x4dA6Df0175C69E52243819dCca59d3B9f640B4A8";

        // Создаем объект контракта
        var myContract = new web3.eth.Contract(contractInterface, contractAddress, {from: account.address});

        var functionData = myContract.methods.sendTokens(count, eth_wallet).encodeABI();

        var privateKey = new Buffer(partPrivKey, 'hex');

        web3.eth.getTransactionCount(account.address).then(function(nonce) {
            console.log('nonce', nonce);
            myContract.methods.sendTokens(count, eth_wallet).estimateGas({from: account.address})
            .then(function(gasAmount){
                console.log('gas amount', gasAmount);
                var gasPriceGwei = 3;
                var gasLimit = 3000000;
                var rawTx = {
                    nonce: web3.utils.toHex(nonce),
                    gasPrice: web3.utils.toHex(gasPriceGwei * 1e9),
                    gasLimit: web3.utils.toHex(gasLimit),
                    to: contractAddress,
                    from: account.address,
                    value: '0x00',
                    chainId: 4, // rinkeby chain
                    data: functionData
                };

                var tx = new Tx(rawTx);
                tx.sign(privateKey);

                var serializedTx = tx.serialize();
                //console.log(serializedTx.toString('hex'));
                web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
                    .on('receipt', console.log);
            })
            .catch(function(error){
                console.log('gas amount error', error);
            });
        });
    }
}

function getCMDprice() {
    return 0.0001;
}

function getExchangeRate(coin, callback) {
    var api_url = '';
    var rate = 0;
    if (coin && typeof callback == 'function') {
        switch (coin) {
            case 'eth':
                rate = 1;
                break;
            case 'teth':
                rate = 1;
                break;
            case 'btc':
                api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=ETH';
                break;
            case 'tbtc':
                api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=ETH';
                break;
            case 'tbch':
                api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/?convert=ETH';
                break;
            case 'bch':
                api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/?convert=ETH';
                break;
            case 'ltc':
                api_url = 'https://api.coinmarketcap.com/v1/ticker/litecoin/?convert=ETH';
                break;
            case 'tltc':
                api_url = 'https://api.coinmarketcap.com/v1/ticker/litecoin/?convert=ETH';
                break;
        }

        if (coin == 'eth' || coin == 'teth') {
            callback(rate);
        } else {
            request(api_url, { json: true }, function (err, res, body) {
                if (err) { return console.log(err); }
                if (typeof body == 'string') {
                    body = JSON.parse(body);
                }

                var first_item = body[0];
                if (first_item && first_item.price_eth) {
                    rate = first_item.price_eth;
                    callback(rate);
                }
            });
        }
    }

    return false;
}

// Функция переподключения к mysql при обрыве соединения
function handleDisconnect() {
    con = mysql.createConnection(db_config);

    con.connect(function(err) {
        if(err) {
            setTimeout(handleDisconnect, 2000);
        }
    });

    con.on('error', function(err) {
        //console.log('db error');
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleDisconnect();
        } else {
            throw err;
        }
    });
}

// Запускаем подключение к БД
handleDisconnect();

// Получение текущей даты в формате Y-m-d H:i:s
function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}