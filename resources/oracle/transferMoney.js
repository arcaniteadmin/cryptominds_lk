/* NodeJS скрипт, запускается по крону, проходится по временным кошелькам, если на балансе есть доступные деньги то переводит их на основной */
var mysql = require('mysql');
require('dotenv').config(); // Библиотека для парсинга из .env файла ларавеля

// Подключаем BitGo
var BitGo = require('bitgo');
var bitgo = new BitGo.BitGo({ env: process.env.bitgo_env, accessToken: process.env.bitgo_token});

var db_config = {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
};

var con; // Переменная для соединения с mysql
var totalWallets = 0; // Переменная для проверки, что проверены все кошельки и можно завершить процесс скрипта
var checkWallets = 0;
handleDisconnect();


// Ждем на всякий случай 1 секунду чтобы соединение с mysql установилось
setTimeout(function () {
    // SQL для получения всех временных кошельков
    var sql_get_wallets = "SELECT * FROM `wallets` ORDER BY `wallets`.`id` DESC";
    con.query(sql_get_wallets, function (err, wallet_result) {
        if (err) throw err;
        totalWallets = wallet_result.length;
        for (var wallet_item_num in wallet_result) {
            var wallet_item = wallet_result[wallet_item_num];
            if (wallet_item.wallet_id) {
                processTransferMoney(wallet_item);
            }
        }
    });
}, 1000);

function processTransferMoney(wallet_item) {
    // Получаем кошелек в битго
    bitgo.coin(wallet_item.coin).wallets().get({ id: wallet_item.wallet_id })
        .then(function(wallet) {
            var balance = wallet.spendableBalance();
            console.log('process wallet '+ wallet_item.wallet_id + ', balance: ' + balance);
            checkWallets++;
            if (balance > 0) {
                var params = {
                    address: getMainWalletAddress(wallet_item.coin),
                    walletPassphrase: wallet_item.passphrase
                };
                bitgo.unlock({ otp: '0000000' })
                    .then(function(unlockResponse) {
                        wallet.sweep(params)
                            .then(function(transactionInfo) {
                                // print transaction info
                                console.log('try transfer money to main wallet');
                                console.dir(transactionInfo);
                                // Сохраняем запись в БД
                                var sql_insert_transaction = "INSERT INTO transaction_main_wallet (user_id,coin,wallet_id,wallet_address,main_wallet_address,passphrase,json_data,created_at,updated_at) VALUES " +
                                "('"+wallet_item.user_id+"', '"+wallet_item.coin+"', '"+wallet_item.wallet_id+"', '"+wallet_item.wallet_address+"', '"+getMainWalletAddress(wallet_item.coin)+"', '"+wallet_item.passphrase+"', '"+JSON.stringify(transactionInfo)+"', '"+getDateTime()+"', '"+getDateTime()+"')";
                                con.query(sql_insert_transaction, function (err, result) {
                                    if (err) throw err;
                                    console.log('Transaction saved success');
                                });
                            });
                    });
            }
            checkToExit();
        });
}

function checkToExit() {
    if (totalWallets == checkWallets) {
        // Если все кошельки проверены то вырубаем скрипт через 5 сек, на случай незаверщенных callback'ов.
        setTimeout(function () {
            console.log('exit script');
            process.exit(1);
        }, 5000);
    }
}

function getMainWalletAddress(coin) {
    var address;
    switch (coin) {
        case 'btc':
            address = process.env.main_bitcoin_wallet;
            break;
        case 'tbtc':
            address = process.env.main_bitcoin_wallet;
            break;
        case 'tbch':
            address = process.env.main_bch_wallet;
            break;
        case 'bch':
            address = process.env.main_bch_wallet;
            break;
        case 'ltc':
            address = process.env.main_ltc_wallet;
            break;
        case 'tltc':
            address = process.env.main_ltc_wallet;
            break;
    }

    return address;
}

// Функция переподключения к mysql при обрыве соединения
function handleDisconnect() {
    con = mysql.createConnection(db_config);

    con.connect(function(err) {
        if(err) {
            setTimeout(handleDisconnect, 2000);
        }
    });

    con.on('error', function(err) {
        //console.log('db error');
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleDisconnect();
        } else {
            throw err;
        }
    });
}

// Запускаем подключение к БД

// Получение текущей даты в формате Y-m-d H:i:s
function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

}