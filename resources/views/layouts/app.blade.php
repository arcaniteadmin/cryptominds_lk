<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">


    <title>Cryptominds</title>

    <!-- Styles UI-->
    <link rel="stylesheet" href="/front_assets/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="/front_assets/css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="/front_assets/css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="/front_assets/css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="/front_assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="/front_assets/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/front_assets/css/main.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/slick.min.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/select2.min.css">
    <link rel="stylesheet" href="/front_assets/css/custom.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/blockui.min.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/pnotify.min.css">
    <link rel="stylesheet" href="/front_assets/css/lib/bootstrap-sweetalert/sweetalert.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/sweet-alert-animations.min.css">

    <link rel="stylesheet" href="/front_assets/css/lib/ion-range-slider/ion.rangeSlider.css">
    <link rel="stylesheet" href="/front_assets/css/lib/ion-range-slider/ion.rangeSlider.skinHTML5.css">
    <link rel="stylesheet" href="/front_assets/css/separate/elements/player.min.css">
    <link rel="stylesheet" href="/front_assets/css/separate/vendor/fancybox.min.css">
    <link rel="stylesheet" href="/front_assets/css/separate/pages/profile-2.min.css">

</head>
<body class="with-side-menu chrome-browser sidebar-hidden">

    @yield('content')

    <!-- Scripts -->
    <script src="/front_assets/js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script src="/front_assets/js/lib/popper/popper.min.js"></script>
    <script src="/front_assets/js/lib/tether/tether.min.js"></script>

    <script src="/front_assets/js/lib/bootstrap/bootstrap.min.js"></script>

    <script src="/front_assets/js/plugins.js"></script>
    <script type="text/javascript" src="/front_assets/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script type="text/javascript" src="/front_assets/js/lib/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/front_assets/js/lib/lobipanel/lobipanel.min.js"></script>
    <script type="text/javascript" src="/front_assets/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/front_assets/js/lib/asPieProgress/jquery-asPieProgress.min.js"></script>
    <script src="/front_assets/js/lib/select2/select2.full.min.js"></script>
    <script src="/front_assets/js/lib/slick-carousel/slick.min.js"></script>
    <script src="/front_assets/js/app.js"></script>
    <script src="/front_assets/js/lib/notie/notie.js"></script>
    <script src="/front_assets/js/lib/notie/notie-init.js"></script>
    <script src="/front_assets/js/lib/pnotify/pnotify.js"></script>
    <script src="/front_assets/js/lib/pnotify/pnotify-init.js"></script>
    <script src="/front_assets/js/lib/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script src="/front_assets/js/lib/bootstrap-notify/bootstrap-notify-init.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>
    <script type="text/javascript" src="/front_assets/js/lib/blockUI/jquery.blockUI.js"></script>
    <script src="/front_assets/js/lib/bootstrap-sweetalert/sweetalert.min.js"></script>
    <script src="/front_assets/js/lib/html5-form-validation/jquery.validation.min.js"></script>



    @section("scripts")
    @show
</body>
</html>
