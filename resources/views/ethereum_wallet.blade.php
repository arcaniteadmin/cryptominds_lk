@extends('layouts.app')

@section('content')
    <header class="site-header">
        <div class="container-fluid">
            <a href="/" class="site-logo">
                <img class="hidden-md-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
                <img class="hidden-lg-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
            </a>

            <button class="hamburger hamburger--htla">
                <span>toggle menu</span>
            </button>
            <div class="site-header-content" style="width: 80%;">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown dropdown-notification messages" style="margin-top: 3px;">
                            <img src="/images/icons8-coins-26.png">
                            <span>{{$countTokens}}</span>
                        </div>
                        <div class="dropdown dropdown-lang">
                            <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="flag-icon flag-icon-us"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-menu-col">
                                    <a class="dropdown-item current" href="#"><span class="flag-icon flag-icon-us"></span>English</a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/front_assets/img/avatar-2-64.png" alt="">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <a class="dropdown-item" href="/profile"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                <a class="dropdown-item" href="/ethereum-wallet"><span class="font-icon glyphicon glyphicon-cog"></span>Ethereum-wallet</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="font-icon glyphicon glyphicon-log-out"></span>Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>

                        <button type="button" class="burger-right">
                            <i class="font-icon-menu-addl"></i>
                        </button>
                    </div><!--.site-header-shown-->

                    <div class="mobile-menu-right-overlay"></div>
                    <div class="site-header-collapsed">

                    </div><!--.site-header-collapsed-->
                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div><!--.container-fluid-->
    </header><!--.site-header-->

    <div class="mobile-menu-left-overlay"></div>
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4"></div>
                <div class="col-xl-4">
                    <div class="box-typical box-typical box-typical-padding">
                        <h5 class="with-border">Edit your ethereum wallet</h5>
                        <form action="{{route('save_ethereum_wallet')}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="form-label semibold">Ethereum wallet address</label>
                                <div class="form-control-wrapper">
                                    <input id="eth_wallet_address" name="address" type="text" class="form-control" value="{{$user->eth_wallet}}" placeholder="address">
                                </div>
                                <div class="eth-wallet-validation-error" style="color: #a94442;">{{session('eth_wallet_err')}}</div>
                            </div>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section("scripts")
    @parent
    <script>
        window.Laravel = {
            'csrfToken': '{{csrf_token()}}'
        };
    </script>
    <script type="text/javascript" src="/js/buy_cmd.js"></script>
    <script>


        (function(){
            new Clipboard('#copy_font_icon_pencil_buy_coin');
        })();
    </script>

@endsection