@extends('layouts.app')

@section('content')

    <header class="site-header">
        <div class="container-fluid">
            <a href="/" class="site-logo">
                <img class="hidden-md-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
                <img class="hidden-lg-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
            </a>

            <button class="hamburger hamburger--htla">
                <span>toggle menu</span>
            </button>
            <div class="site-header-content" style="width: 80%;">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown dropdown-lang">
                            <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="flag-icon flag-icon-us"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-menu-col">
                                    <a class="dropdown-item current" href="#"><span class="flag-icon flag-icon-us"></span>English</a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/front_assets/img/avatar-2-64.png" alt="">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <a class="dropdown-item" href="{{ route('profile') }}"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                <a class="dropdown-item" href="/ethereum-wallet"><span class="font-icon glyphicon glyphicon-cog"></span>Ethereum-wallet</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="font-icon glyphicon glyphicon-log-out"></span>Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>

                        <button type="button" class="burger-right">
                            <i class="font-icon-menu-addl"></i>
                        </button>
                    </div><!--.site-header-shown-->

                    <div class="mobile-menu-right-overlay"></div>
                    <div class="site-header-collapsed">

                    </div><!--.site-header-collapsed-->
                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div><!--.container-fluid-->
    </header><!--.site-header-->

    <div class="mobile-menu-left-overlay"></div>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    @if (Session::has('Success'))
                        <div class="alert alert-success" role="alert">
                            <p class="mb-0">
                                {{ Session::get('Success') }}
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @include('modal.is_coin')
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6">
                    <section class="widget block_price">
                        <header class="widget-header-dark">Token Sale</header>
                        <div class="tab-content widget-tabs-content" style="height: 272px;">

                            <div class="tab-pane active" id="view1-tab-1" role="tabpanel">
                                <center class="block_view1_tab_1 fade">
                                    <div class="tbl-cell tbl-cell-progress">
                                        <div class="circle-progress-bar-typical pieProgress"
                                             role="progressbar" data-goal="10"
                                             data-barcolor="#929faa"
                                             data-barsize="10"
                                             aria-valuemin="0"
                                             aria-valuemax="100">
                                            <span class="pie_progress__number">0%</span>
                                        </div>
                                    </div>
                                    <h4 style="margin-bottom: 2px;margin-top: 8px;">Already collected</h4>
                                    <h4 style="margin-bottom: 0px;padding-bottom: 15px;">306218$</h4>
                                </center>
                            </div>

                            <div class="tab-pane" id="view1-tab-2" role="tabpanel">
                                <center>
                                    <div class="tbl-cell tbl-cell-progress">
                                        <div class="circle-progress-bar-typical pieProgress"
                                             role="progressbar" data-goal="1"
                                             data-barcolor="#929faa"
                                             data-barsize="10"
                                             aria-valuemin="0"
                                             aria-valuemax="100">
                                            <span class="pie_progress__number">0%</span>
                                        </div>
                                    </div>
                                    <h4 style="margin-bottom: 2px;margin-top: 8px;">Already collected</h4>
                                    <h4 style="margin-bottom: 0px;padding-bottom: 15px;">306218$</h4>
                                </center>
                            </div>
                        </div>
                        <div class="widget-tabs-nav bordered">
                            <ul class="tbl-row" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link tab1 active">
                                        <i class="font-icon font-icon-chart-3"></i>
                                        Soft Cap
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tab2">
                                        <i class="font-icon font-icon-notebook-lines"></i>
                                        Hard Cap
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </section><!--.widget-->
                </div>
                <div class="col-xl-6">
                    <section class="widget widget-activity" style="height: 412px;">
                        <div class="container custom_header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 ">
                                    <h3>My Balance</h3>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h3 class="custom_header_float_right">{{$countTokens}}
                                        <span class="custom_header_CMD">CMD</span>
                                    </h3>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                                    <a style="width: 62%;" href="/buy-cmd?testmode=true" class="btn btn-inline btn-rounded btn-primary btn-lg ladda-button">Buy Tokens</a>
                                </div>
                            </div>

                        </div>
                        <h4 style="padding: 20px 20px 0px;margin-bottom: 5px;">Transaction History</h4>
                        <div>
                            <div class="widget-activity-item" style="padding-bottom: 0px;overflow-y: scroll;height: 207px;padding-top: 0px;">
                                <div class="user-card-row">
                                    @if(count($transactions) == 0)
                                    <div class="tbl-row">
                                        <div class="tbl-cell tbl-cell-photo">
                                        </div>
                                        <div class="tbl-cell">
                                            <p>
                                                Your story is empty
                                            </p>
                                        </div>
                                    </div>
                                    @else
                                        <table class="table">
                                            <tbody>
                                        @foreach($transactions as $transaction)
                                                <tr>
                                                    <td style="border: none;">
                                                        <img src="/images/icons8-coins-26.png" alt="" style="-webkit-border-radius: 0%;border-radius: 0%;">
                                                    </td>
                                                    <td style="border: none; padding-left: 0px;">
                                                        <p style="color: black;">
                                                            You've sent {{$transaction->amount}} {{$transaction->coin}} and received {{$transaction->token_count}} CMD
                                                        </p>
                                                    </td>
                                                </tr>
                                        @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </section><!--.widget-tasks-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    <section class="widget widget-tabs-compact">
                        <div class="tab-content widget-tabs-content">
                            <div class="tab-pane active" id="w-4-tab-1" role="tabpanel">
                                <div class="user-card-row">
                                    <div class="tbl-row">
                                        <div class="tbl-cell tbl-cell-photo tbl-cell-photo-64">
                                            <a href="#">
                                                <img src="/front_assets/img/avatar-1-128.png" alt="">
                                            </a>
                                        </div>
                                        <div class="tbl-cell">
                                            <p class="user-card-row-name font-16"><a href="#">{{$user_data->name}}</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="w-4-tab-2" role="tabpanel">
                                <center>Content 2</center>
                            </div>
                            <div class="tab-pane" id="w-4-tab-3" role="tabpanel">
                                <center>Content 3</center>
                            </div>
                        </div>
                        <div class="widget-tabs-nav colored">
                            <ul class="tbl-row" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link orange" target="_blank" href="https://bitcointalk.org/index.php?topic=3038943.msg31270002#msg31270002">
                                        <img src="https://png.icons8.com/material/48/ffffff/conference.png" style="margin: 0 auto;display: block;height: 48px;">
                                        Bounty program
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link blue"  target="_blank" href="https://medium.com/@CryptoMindsMe/this-is-blitz-98ff675c15d7">
                                        <img src="https://png.icons8.com/ios/64/ffffff/help.png" style="margin: 0 auto;display: block;height: 48px;">
                                        FAQ
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link red" href="/ethereum-wallet">
                                        <img src="https://png.icons8.com/windows/64/ffffff/ethereum.png" style="margin: 0 auto;display: block;height: 48px;">
                                        Ethereum-wallet
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <div class="col-xl-6">
                    <section class="widget widget-time">
                        <header class="widget-header-dark with-btn">
                            Private sale ends in!
                        </header>
                        <div class="widget-time-content" style="height: 137px;padding-top: 40px;">
                            <div class="timer">
                                <div class="count-item">
                                    <div class="count-item-number">
                                        <span id="days"></span>
                                        <div class="count-item-caption" >day</div>
                                    </div>
                                </div>
                                <div class="count-item divider">:</div>
                                <div class="count-item">
                                    <div class="count-item-number">
                                        <span id="hours"></span>
                                        <div class="count-item-caption" >hour</div>
                                    </div>
                                </div>
                                <div class="count-item divider">:</div>
                                <div class="count-item">
                                    <div class="count-item-number">
                                        <span id="minutes"></span>
                                        <div class="count-item-caption" >min</div>
                                    </div>
                                </div>
                                <div class="count-item divider">:</div>
                                <div class="count-item">
                                    <div class="count-item-number">
                                        <span id="seconds"></span>
                                        <div class="count-item-caption" >sec</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('js/pages/home.js') }}"></script>
@endsection
