@extends('layouts.app')

@section('content')
    <header class="site-header">
        <div class="container-fluid">
            <a href="/" class="site-logo">
                <img class="hidden-md-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
                <img class="hidden-lg-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
            </a>

            <button class="hamburger hamburger--htla">
                <span>toggle menu</span>
            </button>
            <div class="site-header-content" style="width: 80%;">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown dropdown-notification messages" style="margin-top: 3px;">
                            <img src="/images/icons8-coins-26.png">
                            <span>{{$countTokens}}</span>
                        </div>
                        <div class="dropdown dropdown-lang">
                            <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="flag-icon flag-icon-us"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-menu-col">
                                    <a class="dropdown-item current" href="#"><span class="flag-icon flag-icon-us"></span>English</a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/front_assets/img/avatar-2-64.png" alt="">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <a class="dropdown-item" href="/ethereum-wallet"><span class="font-icon glyphicon glyphicon-cog"></span>Ethereum-wallet</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="font-icon glyphicon glyphicon-log-out"></span>Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>

                        <button type="button" class="burger-right">
                            <i class="font-icon-menu-addl"></i>
                        </button>
                    </div><!--.site-header-shown-->

                    <div class="mobile-menu-right-overlay"></div>
                    <div class="site-header-collapsed">

                    </div><!--.site-header-collapsed-->
                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div><!--.container-fluid-->
    </header><!--.site-header-->

    <div class="mobile-menu-left-overlay"></div>

    <div class="page-content">
        <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-4">
                        @include('modal.is_coin')
                    </div>
                    <div class="col-xl-4">
                        <div class="box-typical">
                            <div style="padding:20px 15px;border-bottom:solid 1px #c5d6de;">
                                <h3 style="font-weight: bold;">Choose payment method</h3>
                                <p style="text-decoration:underline;text-align:center;margin-bottom: 0px;">Cryptocurrency</p>
                            </div>
                            <div style="padding:20px 15px;border-bottom:solid 1px #c5d6de; ">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="eth_wallet_address">Choose cryptocurrency</label>
                                    <select class="form-control select-crypto-currency">
                                        <option value="empty">Choose cryptocurrency</option>
                                        @if(isset($_GET['testmode']))
                                            <option value="tbtc">Bitcoin (Testnet)</option>
                                            <option value="tbch">Bitcoin Cash (Testnet)</option>
                                            <option value="teth">Ethereum (Testnet Kovan)</option>
                                            <option value="tltc">Litecoin (Testnet)</option>
                                        @else
                                            <option value="btc">Bitcoin</option>
                                            <option value="bch">Bitcoin Cash</option>
                                            <option value="eth">Ethereum</option>
                                            <option value="ltc">Litecoin</option>
                                            <option value="btg">Bitcoin Gold</option>
                                        @endif
                                    </select>
                                    <!--<div class="loader" style="text-align: center; display: none"><img src="/images/loader.gif" height="48" /></div>
                                    <div class="alert alert-info success-alert-box" role="alert" style="display: none"></div>
                                    <div class="alert alert-danger error-alert-box" role="alert" style="display: none"></div>-->
                                </fieldset>
                            </div>
                            <div class="loader" style="text-align: center; display: none"><img src="/images/loader.gif" height="48" /></div>
                            <div id="check_buy_coin">
                                <div style="border-bottom:solid 1px #c5d6de;">
                                    <section class="widget widget-time" style="margin-bottom: 0px;">
                                        <header class="widget-header-dark with-btn text-center" style="border-radius: 0px;padding-right: 16px;background: black;">
                                            Enter ammount in <span class="span-coin-label">BTC</span>
                                        </header>
                                        <div class="widget-time-content" style="padding-top: 50px;padding-bottom: 50px;background: white;border-radius: 0px; border: 0px;">
                                            <div class="count-item">
                                                <img id="is_minus_buy_coin" style="width: 30px;margin-top: 13px;" src="https://png.icons8.com/ios/50/000000/minus-filled.png">
                                            </div>
                                            <div class="count-item divider"></div>
                                            <div class="count-item" style="width: 40%;">
                                                <div  class="count-item-number" style="width: 100%;"><input id="is_number_buy_coin"
                                                            type="number" style="font-size: 28px;width: 100%; text-align: center;border: none;background: white;" value="0.01"></div>
                                            </div>
                                            <div class="count-item divider"></div>
                                            <div class="count-item">
                                                <img id="is_plus_buy_coin" style="width: 30px;margin-top: 13px;" src="https://png.icons8.com/ios/50/000000/plus-filled.png">
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div>
                                    <section class="widget widget-time" style="margin-bottom: 0px;">
                                        <header class="widget-header-dark with-btn text-center" style="font-size: 14px;background: black; border-radius: 0px;padding-right: 16px;">
                                            Your are getting CMD
                                        </header>
                                        <div class="widget-time-content" style="padding-top: 0px;border-radius: 0px;background: black;border: 0px;">

                                            <div class="count-item">
                                                <div id="calculate_cmd_number_buy_coin" class="count-item-number" style="color: white;font-size: 28px;"></div>
                                            </div>

                                        </div>
                                    </section>
                                </div>
                                <div>
                                    <section class="widget widget-time" style="margin-bottom: 0px;">
                                        <div class="widget-time-content" style="border-radius: 0px; border: 0px;padding-bottom: 0px;">
                                            <img id="img_QrCode_buy_coin" src="" alt="">
                                        </div>
                                    </section>
                                </div>
                                <div style="padding:20px 15px;border-bottom:solid 1px #c5d6de; ">
                                    <label class="form-label semibold">Wallet adress</label>
                                    <div class="form-control-wrapper form-control-icon-right">
                                        <input id="wallet_adress_buy_coin" type="text" class="form-control form-control-sm alert alert-info success-alert-box">
                                        <i id="copy_font_icon_pencil_buy_coin" class="font-icon font-icon-pencil" data-clipboard-target="#wallet_adress_buy_coin"></i>
                                    </div>
                                </div>
                                <div style="border-bottom:solid 1px #c5d6de;">
                                    <section class="widget widget-time" style="margin-bottom: 0px;">
                                        <header class="widget-header-dark with-btn text-center" style="background: white; color: black; border-radius: 0px;padding-right: 16px;">
                                            Your funds are expected to be recieved
                                        </header>
                                        <div class="widget-time-content" style="text-align: left;padding: 0px;border-radius: 0px; border: 0px;">
                                            <section id="blockui-element-container-default" class="card" style="margin-bottom: 0px; border: none;">
                                                <div class="card-block display-table" style="min-height: 100px">

                                                </div>
                                            </section>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="alert alert-danger error-alert-box" role="alert" style="display: none"></div>

                        </div>
                    </div>
                </div>
        </div>
    </div>


@endsection

@section("scripts")
    @parent
    <script>
        window.Laravel = {
            'csrfToken': '{{csrf_token()}}'
        };

        //ajax ззапрос формы модального окна is_coin
        $('#submit_is_coin').on('click', function(e) {
            e.preventDefault();
            var url = window.location.origin + '/save-ethereum-wallet_ajax';
            var address = $('#eth_wallet_address').val();
            var token = $('#token').contents().prevObject["0"].content;
            var data = {
                'address': address,
                '_token': token
            };
            $.post(url, data, function (response) {
                if(response.status == true) {
                    notie.alert_show(1, 'Success.', 3);
                    setTimeout(function () {
                        notie.alert_hide();
                        window.location.replace(window.location.origin);
                    }, 1500);
                } else if (response.adress == 'NULL') {
                    $('#form_group_is_coin').addClass('error');
                    $('#eth_wallet_address').val('');
                    notie.alert_show(3, 'Address can not be empty.', 3);
                    setTimeout(function () {
                        notie.alert_hide();
                    }, 1500);
                } else {
                    $('#form_group_is_coin').addClass('error');
                    $('#eth_wallet_address').val('');
                    notie.alert_show(3, 'Wrong ethereum address.', 3);
                    setTimeout(function () {
                        notie.alert_hide();
                    }, 1500);
                }
            })
        });

        //При фокусе инпута убираются стили ошибки
        $('#eth_wallet_address').focus(function() {
            $('#form_group_is_coin').removeClass('error');
        });
    </script>
    <script type="text/javascript" src="/js/buy_cmd.js"></script>
    <script>

        (function(){
            new Clipboard('#copy_font_icon_pencil_buy_coin');
        })();
    </script>

@endsection
