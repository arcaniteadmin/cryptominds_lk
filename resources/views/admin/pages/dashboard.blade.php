@extends('admin.layouts.app')
@section('title')
    Панель управления
@stop
@section('styles')
    @parent
@stop

@section('content')
    <nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
        <span class="cat__core__title d-block mb-2">
        <strong>Контрольная панель</strong>
        <small class="text-muted"></small>
    </span>
    </nav>
    <div class="row">
        <div class="col-lg-3">
            <div class="cat__core__widget">
                <div class="cat__core__step cat__core__step--success">
                <span class="cat__core__step__digit">
                    <i class="icmn-database"><!-- --></i>
                </span>
                    <div class="cat__core__step__desc">
                        <span class="cat__core__step__title">Bitcoin</span>
                        <p>За сегодня: 50</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="cat__core__widget">
                <div class="cat__core__step cat__core__step--primary">
                <span class="cat__core__step__digit">
                    <i class="icmn-users"><!-- --></i>
                </span>
                    <div class="cat__core__step__desc">
                        <span class="cat__core__step__title">Bitcoin Cash</span>
                        <p>За сегодня: 10</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="cat__core__widget">
                <div class="cat__core__step cat__core__step--danger">
                <span class="cat__core__step__digit">
                    <i class="icmn-bullhorn"><!-- --></i>
                </span>
                    <div class="cat__core__step__desc">
                        <span class="cat__core__step__title">Litecoin</span>
                        <p>За сегодня: 3</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="cat__core__widget">
                <div class="cat__core__step cat__core__step--default">
                <span class="cat__core__step__digit">
                    <i class="icmn-price-tags"><!-- --></i>
                </span>
                    <div class="cat__core__step__desc">
                        <span class="cat__core__step__title">Ethereum</span>
                        <p>За сегодня: 50</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="cat__core__widget">
                <p class="pt-3 px-3"><strong>Собрано за все время:</strong></p>
                <div class="chart-line height-300 chartist"></div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function(){

            function loadGame() {
                $('#hidden-game').attr('src', $('#hidden-game').attr('load-src'));
            }

            $('#duck-game .card-header').on('dblclick', function(){
                loadGame();
            });

            $('#duck-game .cat__core__sortable__uncollapse').on('click', function(){
                loadGame();
            });

        });
    </script>
    <script>
        $( function() {

            ///////////////////////////////////////////////////////////
            // tooltips
            $("[data-toggle=tooltip]").tooltip();

            ///////////////////////////////////////////////////////////
            // chart1
            new Chartist.Line(".chart-line", {
                labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
                series: [
                    [5, 0, 7, 8, 12],
                    [2, 1, 3.5, 7, 3],
                    [1, 3, 4, 5, 6]
                ]
            }, {
                fullWidth: !0,
                chartPadding: {
                    right: 40
                },
                plugins: [
                    Chartist.plugins.tooltip()
                ]
            });

        } );
    </script>
@endsection