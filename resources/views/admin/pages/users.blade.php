@extends('admin.layouts.app')
@section('title')
    Панель управления
@stop
@section('styles')
    @parent
@stop

@section('content')
    <nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
        <span class="cat__core__title d-block mb-2">
        <strong>Пользователи</strong>
        <small class="text-muted"></small>
    </span>
    </nav>
    <section class="card">
        <div class="card-header">
        <span class="cat__core__title">
            <strong>Список пользователей</strong>
        </span>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-12">
                    <div class="mb-0">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Имя</th>
                                <th>Email</th>
                                <th>Eth_wallet</th>
                                <th>Временные кошельки</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade modal-size-large" id="example2" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <section class="card">
                        <div class="card-header">
                            <span class="cat__core__title">
                                <strong>Список транзакций</strong>
                            </span>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-0">
                                        <table class="table table-hover nowrap" id="example3" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Криптовалюта</th>
                                                <th>Сумма</th>
                                                <th>Курс обмена</th>
                                                <th>Eth сумма</th>
                                                <th>Кол-во токенов</th>
                                                <th>Дата</th>
                                                <th>ID кошелька bitgo</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function(){
            $('#example1').DataTable({
                responsive: true,
                serverSide: true,
                ajax: window.location.origin + '/ajax_users',
                columns: [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "email"},
                    {"data": "eth_wallet"},
                    {"data": "eth_wallet"}
                ],
                "aoColumnDefs": [
                    {
                        "aTargets": [4],
                        "mData": "",
                        "mRender": function (data, type, row) {
                            return "<a class='btn btn-primary mr-2 mb-2 btn-sm show_modal' id='" + row.id + "'>Посмотреть</a>";

                        }
                    }
                ],
                "oLanguage": {
                    "sEmptyTable": "Данных нет",
                    "sInfo": "Всего _TOTAL_  (_START_ по _END_)",
                    "sSearch": "Поиск:",
                    "sLengthMenu": "Показать _MENU_ строк",
                    "oPaginate": {
                        "sPrevious": "Пред.",
                        "sNext": "След."
                    }
                }
            });

        });

        var wallet_user_id = null;

        $('body').on('click', '.show_modal', function(e) {
             var id = $(e.currentTarget).attr('id');
             wallet_user_id = id;


             $('#example2').modal('show');
        });

        $('#example2').on('show.bs.modal', function () {

            $('#example3').DataTable({
                responsive: true,
                serverSide: true,
                ajax: window.location.origin + '/wallet_users/' + wallet_user_id,
                columns: [
                    {"data": "coin"},
                    {"data": "amount"},
                    {"data": "rate"},
                    {"data": "eth_amount"},
                    {"data": "token_count"},
                    {"data": "created_at"},
                    {"data": "wallet_id"}
                ],
                "oLanguage": {
                    "sZeroRecords": "Данных нет",
                    "sInfoEmpty": "Данных нет",
                    "sEmptyTable": "Данных нет",
                    "sInfo": "Всего _TOTAL_  (_START_ по _END_)",
                    "sSearch": "Поиск:",
                    "sLengthMenu": "Показать _MENU_ строк",
                    "oPaginate": {
                        "sPrevious": "Пред.",
                        "sNext": "След."
                    }
                }
            });
        });
        $('#example2').on('hidden.bs.modal', function (e) {
            var table = $('#example3').DataTable();
            table
                .clear();
            $('#example3').dataTable().fnDestroy();
        });
    </script>
@endsection