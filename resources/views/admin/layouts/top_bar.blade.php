<div class="cat__top-bar">
    <!-- left aligned items -->
    <div class="cat__top-bar__left">
        <div class="cat__top-bar__logo">
            <a href="{{url('/dashboard')}}">
                <img src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" style="max-height: 38px !important;"/>
            </a>
        </div>
        <div class="cat__top-bar__item hidden-sm-down">
            <div class="dropdown">
                <a href="{{url('/dashboard')}}">
                    <i class="icmn-home mr-2"></i>
                    <span><strong>Контрольная панель</strong></span>
                </a>
            </div>
        </div>
        <div class="cat__top-bar__item hidden-lg-down">
            <div class="cat__top-bar__search">
                <i class="icmn-search"><!-- --></i>
                <input type="text" placeholder="Поиск..." />
            </div>
        </div>
    </div>
    <!-- right aligned items -->
    <div class="cat__top-bar__right">
        <div class="cat__top-bar__item">
            <div class="dropdown cat__top-bar__avatar-dropdown">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="cat__top-bar__avatar" href="javascript:void(0);">
                        <img src="../../admin_assets/modules/dummy-assets/common/img/avatars/admin.png"/>
                    </span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon icmn-user"></i> Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{url('/logout')}}"><i class="dropdown-icon icmn-exit"></i> Logout</a>
                </ul>
            </div>
        </div>
        <div class="cat__top-bar__item">
            <div class="cat__top-bar__menu-button cat__menu-right__action--menu-toggle">
                <i class="fa fa-bars"><!-- --></i>
            </div>
        </div>
    </div>
</div>