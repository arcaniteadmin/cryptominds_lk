<nav class="cat__menu-left">
    <div class="cat__menu-left__lock cat__menu-left__action--menu-toggle">
        <div class="cat__menu-left__pin-button">
            <div><!-- --></div>
        </div>
    </div>
    <div class="cat__menu-left__logo">
        <a href="{{url('/dashboard')}}">
            <img src="../../admin_assets/modules/pages/common/img/logo-inverse.png" />
        </a>
    </div>
    <div class="cat__menu-left__inner">
        <ul class="cat__menu-left__list cat__menu-left__list--root">
            <li class="cat__menu-left__item">
                <a href="{{url('/users')}}">
                    <span class="cat__menu-left__icon icmn-file-text"></span>
                    Пользователи
                </a>
            </li>
            <li class="cat__menu-left__item">
                <a href="#">
                    <span class="cat__menu-left__icon icmn-stack"></span>
                    Транзакции за сегодня
                </a>
            </li>
        </ul>
    </div>
</nav>