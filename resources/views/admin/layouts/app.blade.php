<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        @section('title')
        @show
    </title>

    @section('styles')
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/ladda/dist/ladda-themeless.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/select2/dist/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/summernote/dist/summernote.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/ionrangeslider/css/ion.rangeSlider.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/datatables/media/css/dataTables.bootstrap4.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/c3/c3.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/chartist/dist/chartist.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/nprogress/nprogress.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/jquery-steps/demo/css/jquery.steps.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/dropify/dist/css/dropify.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/font-linearicons/style.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/font-icomoon/style.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/cleanhtmlaudioplayer/src/player.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/cleanhtmlvideoplayer/src/player.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/core/common/core.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/vendors/common/vendors.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/layouts/common/layouts-pack.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/themes/common/themes.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/menu-left/common/menu-left.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/menu-right/common/menu-right.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/top-bar/common/top-bar.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/footer/common/footer.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/pages/common/pages.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/ecommerce/common/ecommerce.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets/modules/apps/common/apps.cleanui.css">
@show

<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="cat__config--vertical cat__menu-left--colorful">

@include('admin.layouts.left_menu')
@include('admin.layouts.right_menu')
@include('admin.layouts.top_bar')

<div class="cat__content">
    @yield('content')
</div>

@section('scripts')
    <script src="/admin_assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="/admin_assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="/admin_assets/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="/admin_assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="/admin_assets/vendors/spin.js/spin.js"></script>
    <script src="/admin_assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/admin_assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="/admin_assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="/admin_assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="/admin_assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="/admin_assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="/admin_assets/vendors/moment/min/moment.min.js"></script>
    <script src="/admin_assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/admin_assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="/admin_assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="/admin_assets/vendors/summernote/dist/summernote.min.js"></script>
    <script src="/admin_assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="/admin_assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="/admin_assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="/admin_assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/admin_assets/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="/admin_assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="/admin_assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="/admin_assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="/admin_assets/vendors/d3/d3.min.js"></script>
    <script src="/admin_assets/vendors/c3/c3.min.js"></script>
    <script src="/admin_assets/vendors/chartist/dist/chartist.min.js"></script>
    <script src="/admin_assets/vendors/peity/jquery.peity.min.js"></script>
    <script src="/admin_assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="/admin_assets/vendors/jquery-countTo/jquery.countTo.js"></script>
    <script src="/admin_assets/vendors/nprogress/nprogress.js"></script>
    <script src="/admin_assets/vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/admin_assets/vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="/admin_assets/vendors/dropify/dist/js/dropify.min.js"></script>
    <script src="/admin_assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="/admin_assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="/admin_assets/modules/menu-left/common/menu-left.cleanui.js"></script>
    <script src="/admin_assets/modules/menu-right/common/menu-right.cleanui.js"></script>
@show
</body>
</html>