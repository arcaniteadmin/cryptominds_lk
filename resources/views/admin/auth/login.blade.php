<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cryptominds Manager Auth</title>
    <link href="/admin_assets/modules/core/common/img/favicon.ico" rel="shortcut icon">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">

    <!-- VENDORS -->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/datatables/media/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/chartist/dist/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/jquery-steps/demo/css/jquery.steps.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/font-linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/font-icomoon/style.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/cleanhtmlaudioplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/vendors/cleanhtmlvideoplayer/src/player.css">
    <script src="/admin_assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="/admin_assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="/admin_assets/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/admin_assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="/admin_assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="/admin_assets/vendors/spin.js/spin.js"></script>
    <script src="/admin_assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/admin_assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="/admin_assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="/admin_assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="/admin_assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="/admin_assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="/admin_assets/vendors/moment/min/moment.min.js"></script>
    <script src="/admin_assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/admin_assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/admin_assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="/admin_assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="/admin_assets/vendors/summernote/dist/summernote.min.js"></script>
    <script src="/admin_assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="/admin_assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="/admin_assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="/admin_assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/admin_assets/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="/admin_assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="/admin_assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="/admin_assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="/admin_assets/vendors/d3/d3.min.js"></script>
    <script src="/admin_assets/vendors/c3/c3.min.js"></script>
    <script src="/admin_assets/vendors/chartist/dist/chartist.min.js"></script>
    <script src="/admin_assets/vendors/peity/jquery.peity.min.js"></script>
    <script src="/admin_assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="/admin_assets/vendors/jquery-countTo/jquery.countTo.js"></script>
    <script src="/admin_assets/vendors/nprogress/nprogress.js"></script>
    <script src="/admin_assets/vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/admin_assets/vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="/admin_assets/vendors/dropify/dist/js/dropify.min.js"></script>
    <script src="/admin_assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="/admin_assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>

    <!-- CLEAN UI ADMIN TEMPLATE MODULES-->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/core/common/core.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/vendors/common/vendors.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/layouts/common/layouts-pack.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/themes/common/themes.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/menu-left/common/menu-left.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/menu-right/common/menu-right.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/top-bar/common/top-bar.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/footer/common/footer.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/pages/common/pages.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/ecommerce/common/ecommerce.cleanui.css">
    <link rel="stylesheet" type="text/css" href="/admin_assets/modules/apps/common/apps.cleanui.css">
    <script src="/admin_assets/modules/menu-left/common/menu-left.cleanui.js"></script>
    <script src="/admin_assets/modules/menu-right/common/menu-right.cleanui.js"></script>
</head>

<body class="cat__config--vertical cat__menu-left--colorful">
<div class="cat__content">

    <!-- START: pages/login-alpha -->
    <div class="cat__pages__login cat__pages__login--fullscreen" style="background-image: url(/admin_assets/modules/pages/common/img/login/2.jpg)">
        <div class="cat__pages__login__header">
            <div class="row">
                <div class="col-lg-8">
                    <div class="cat__pages__login__header__logo">
                        <a>
                            <img src="../../admin_assets/modules/pages/common/img/logo-inverse.png" alt="Cryptominds" style="max-width: 17.71rem;">
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="cat__pages__login__header__menu">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="javascript: void(0);">About</a></li>
                            <li class="list-inline-item"><a href="javascript: void(0);">Support</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="cat__pages__login__block">
            <div class="row">
                <div class="col-xl-12">
                    <div class="cat__pages__login__block__promo text-white text-center">
                        <h1 class="mb-3">
                            <strong>Добро пожаловать</strong>
                        </h1>
                    </div>
                    <div class="cat__pages__login__block__inner">
                        <div class="cat__pages__login__block__form">
                            <h4 class="text-uppercase">
                                <strong>Авторизуйтесь пожалуйста</strong>
                            </h4>
                            <br />
                            <form id="form-validation" action="{{url('/login_attemp')}}" name="form-validation" method="POST">
                                <div class="form-group">
                                    <label class="form-label">Логин</label>
                                    <input id="validation-email"
                                           class="form-control"
                                           placeholder="Ваш логин"
                                           name="validation[login]"
                                           type="email"
                                           data-validation="[EMAIL]"
                                           data-validation-message="Логин заполнен неправильно">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Пароль</label>
                                    <input id="validation-password"
                                           class="form-control password"
                                           name="validation[password]"
                                           type="password"
                                           placeholder="Ваш пароль"
                                           data-validation="[L>=6]"
                                           data-validation-message="Пароль не может быть менее 6 символов">
                                </div>
                                <div class="error" style="color:#FF0000">{!!Session::get('auth_error')!!}</div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary mr-3">Войти</button>
                                    <span class="register-link">
                                </span>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cat__pages__login__footer text-center">
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item"><a href="javascript: void(0);">Terms of Use</a></li>
                <li class="active list-inline-item"><a href="javascript: void(0);">Compliance</a></li>
                <li class="list-inline-item"><a href="javascript: void(0);">Confidential Information</a></li>
                <li class="list-inline-item"><a href="javascript: void(0);">Support</a></li>
                <li class="list-inline-item"><a href="javascript: void(0);">Contacts</a></li>
            </ul>
        </div>
    </div>
    <!-- END: pages/login-alpha -->

    <!-- START: page scripts -->
    <script>
        $(function() {

            // Form Validation
            $('#form-validation').validate({
                submit: {
                    settings: {
                        inputContainer: '.form-group',
                        errorListClass: 'form-control-error',
                        errorClass: 'has-danger'
                    }
                }
            });

            // Show/Hide Password
            $('.password').password({
                eyeClass: '',
                eyeOpenClass: 'icmn-eye',
                eyeCloseClass: 'icmn-eye-blocked'
            });

            // Switch to fullscreen
            $('.switch-to-fullscreen').on('click', function () {
                $('.cat__pages__login').toggleClass('cat__pages__login--fullscreen');
            });

            // Change BG
            $('.random-bg-image').on('click', function () {
                var min = 1, max = 5,
                    next = Math.floor($('.random-bg-image').data('img')) + 1,
                    final = next > max ? min : next;

                $('.random-bg-image').data('img', final);
                $('.cat__pages__login').data('img', final).css('backgroundImage', 'url(../../modules/pages/common/img/login/' + final + '.jpg)');
            })

        });
    </script>

</div>
<script src="/admin_assets/js/auth.js"></script>
</body>
</html>