<div id="is_coin_modal" class="modal fade bd-example-modal-lg"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Ethereum</h4>
            </div>
            <form action="" method="POST">
                <div class="modal-body">
                    <div class="container">
                        {{csrf_field()}}
                        <fieldset class="form-group" id="form_group_is_coin">
                            <label class="form-label semibold" for="exampleInput">Enter your ETH wallet</label>
                            <input class="form-control" id="eth_wallet_address" name="address" type="text" class="form-control" value="" placeholder="Address" required>
                            <div class="error-list" data-error-list=""><ul><li>If you dont have any wallet you can <a
                                                target="_blank" href="https://www.myetherwallet.com/">register it.</a></li></ul></div>
                        </fieldset>
                    </div>
                </div>
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
                    <button id="submit_is_coin" type="submit" class="btn btn-rounded btn-success">Save</button>
                </div>
        </div>
    </div>
</div>

