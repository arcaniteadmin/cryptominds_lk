@extends('layouts.app')

@section('content')

    <header class="site-header">
        <div class="container-fluid">
            <a href="/" class="site-logo">
                <img class="hidden-md-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
                <img class="hidden-lg-down" src="../../admin_assets/modules/pages/common/img/logo_cryptominds.png" alt="">
            </a>

            <button class="hamburger hamburger--htla">
                <span>toggle menu</span>
            </button>
            <div class="site-header-content" style="width: 80%;">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown dropdown-notification messages" style="margin-top: 3px;">
                            <img src="/images/icons8-coins-26.png">
                            <span>{{$countTokens}}</span>
                        </div>
                        <div class="dropdown dropdown-lang">
                            <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="flag-icon flag-icon-us"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-menu-col">
                                    <a class="dropdown-item current" href="#"><span class="flag-icon flag-icon-us"></span>English</a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/front_assets/img/avatar-2-64.png" alt="">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <a class="dropdown-item" href="/profile"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                <a class="dropdown-item" href="/ethereum-wallet"><span class="font-icon glyphicon glyphicon-cog"></span>Ethereum-wallet</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="font-icon glyphicon glyphicon-log-out"></span>Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>

                        <button type="button" class="burger-right">
                            <i class="font-icon-menu-addl"></i>
                        </button>
                    </div><!--.site-header-shown-->

                    <div class="mobile-menu-right-overlay"></div>
                    <div class="site-header-collapsed">

                    </div><!--.site-header-collapsed-->
                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div><!--.container-fluid-->
    </header><!--.site-header-->

    <div class="mobile-menu-left-overlay"></div>
    <div class="page-content">

        <div class="container-fluid">
            <header class="section-header">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell">
                            <h3>Your email adress</h3>
                            <ol class="breadcrumb breadcrumb-simple">
                                <li>{{ $user->email }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </header>
            <div class="row">
                <div class="col-lg-8">
                    <section class="tabs-section">
                        @if(Session('success_email'))
                            <div class="alert alert-success" role="alert">
                                {{Session('success_email')}}
                            </div>
                        @endif
                        @if(Session('success_password'))
                            <div class="alert alert-success" role="alert">
                                {{Session('success_password')}}
                            </div>
                        @endif
                        <div class="tabs-section-nav tabs-section-nav-left">
                            <ul class="nav" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#tabs-2-tab-1" role="tab" data-toggle="tab">
                                        <span class="nav-link-in">Сhange password</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tabs-2-tab-2" role="tab" data-toggle="tab">
                                        <span class="nav-link-in">Сhange email</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!--.tabs-section-nav-->
                        <div class="tab-content no-styled profile-tabs">
                            <div role="tabpanel" class="tab-pane active" id="tabs-2-tab-1">
                                <section class="box-typical box-typical-padding">
                                        <h5 class="with-border">Сhange password</h5>

                                        <form id="check_password" action="{{ route('change_password') }}" method="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <fieldset class="form-group pw1">
                                                <label class="form-label">New password</label>
                                                <input  name="signup_v1[password]" data-validation="[L>=6]" type="password" class="form-control" minlength="6">
                                            </fieldset>
                                            <fieldset class="form-group pw2">
                                                <label class="form-label">Confirm password</label>
                                                <input data-validation-message="$ does not match the password" name="signup_v1[password-confirm]" data-validation="[V==signup_v1[password]]" type="password" class="form-control" minlength="6">

                                            </fieldset>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn">Submit</button>
                                            </fieldset>
                                        </form>

                                </section>
                            </div><!--.tab-pane-->
                            <div role="tabpanel" class="tab-pane" id="tabs-2-tab-2">
                                <section class="box-typical box-typical-padding">
                                    <h5 class="with-border">Сhange email</h5>
                                    <form id="check_email" action="{{ route('change_email') }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <fieldset class="form-group pw1">
                                            <label class="form-label">New email</label>
                                            <input id="email" name="email[email]" data-validation="[EMAIL]" type="email" class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <button type="submit" class="btn">Submit</button>
                                        </fieldset>
                                    </form>
                                </section>
                            </div><!--.tab-pane-->
                        </div><!--.tab-content-->
                    </section><!--.tabs-section-->
                </div>
            </div><!--.row-->
        </div><!--.container-fluid-->
    </div><!--.page-content-->

@endsection
@section('scripts')
    @parent


    <script>
        $('#check_password').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group'
                }
            }
        });
        $('#check_email').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group'
                }
            }
        });
    </script>
    <!--<script src="/js/pages/profile.js"></script>-->
@endsection
