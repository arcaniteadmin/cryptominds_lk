$(document).ready(function () {

    var selected_rate = 0;
    var cmd_rate = 0;
    var selected_crypto_address = null;
    var open_page_datetime = getDateTime();


    $('.select-crypto-currency').change(function (e) {
        var currency = $(e.currentTarget).val();
        if (currency != 'empty') {
            var url = window.location.origin + '/generate-temporary-wallet';
            $('.alert').hide();
            show_loader();
            $.post(url, {currency: currency, _token: window.Laravel.csrfToken}, function (response) {
                console.log(response);
                hide_loader();
                if (response.status) {
                    if (response.data) {
                        if (response.data.error) {
                            show_error(response.data.message);
                        } else {
                            // Set selected cryptocurrency exchange rate
                            selected_crypto_address = response.data.wallet;
                            $('.span-coin-label').text(getCoinLabel(currency));
                            getExchangeRate(currency, function (rate) {
                               selected_rate = parseFloat(rate);
                               calculator.setMinPrice();
                            });
                            show_success(response.data.wallet);
                            $('#check_buy_coin').show();

                            open_page_datetime = getDateTime();
                            initDetectTransactions(response.data.wallet);
                            $('#wallet_adress_buy_coin').val(response.data.wallet);
                            $('#img_QrCode_buy_coin').attr("src", getQrCodeImageUrl());
                            block_circle_buy_coin();
                        }
                    }
                } else {
                    if (response.message == 'missing_eth_address') {
                        openEthereumModal();
                    } else {
                        show_error('Oops! Error at created temporary wallet. Please try again later');
                    }
                }
            });
        }
    });

    // Калькулятор
    var calculator = {
        min_tokens_sum: 6000.000000001,
        step: 0.001,
        input: $("#is_number_buy_coin"),
        plusBtn: $('#is_plus_buy_coin'),
        minusBtn: $('#is_minus_buy_coin'),
        tokenCountLabel: $('#calculate_cmd_number_buy_coin'),

        initialize: function () {
            var that = this;
            this.input.bind("change paste keyup", function () {
                that.changeEvent();
            });

            this.plusBtn.on('click', function() {
                that.plusEvent();
            });

            this.minusBtn.on('click', function() {
                that.minusEvent();
            });
        },

        plusEvent: function () {
            var amount = this.input.val();
            if (amount && amount != "") {
                amount = amount.replace(',', '.');
                amount = parseFloat(amount);
                amount += this.step;
                this.input.val(amount);
                var token_count = calculate_cmd(amount);
                this.tokenCountLabel.text(token_count.toFixed(8));
                this.checkAmountError(amount);
            }
        },

        minusEvent: function () {
            var amount = this.input.val();
            if (amount && amount != "") {
                amount = amount.replace(',', '.');
                amount = parseFloat(amount);
                if (amount >= this.step) {
                    amount -= this.step;
                    this.input.val(amount);
                    var token_count = calculate_cmd(amount);
                    this.tokenCountLabel.text(token_count.toFixed(8));
                }
                this.checkAmountError(amount);
            }
        },

        changeEvent: function () {
            var amount = this.input.val();
            if (amount && amount != "") {
                amount = amount.replace(',', '.');
                amount = parseFloat(amount);
                this.checkAmountError(amount);
                var token_count = calculate_cmd(amount);
                this.tokenCountLabel.text(token_count.toFixed(8));

            }
        },

        setMinPrice: function () {
            this.input.val(this.getMinCryptoAmount());
            this.input.change();
        },

        checkAmountError: function (amount) {
            if (amount < this.getMinCryptoAmount()) {
                this.showErrorAmount();
            } else {
                this.hideErrorAmount();
            }
        },

        getMinCryptoAmount: function () {
            var minEthAmount = this.min_tokens_sum * cmd_rate;
            var cryptoAmount = minEthAmount / selected_rate;
            return cryptoAmount;
        },

        showErrorAmount: function () {
            this.tokenCountLabel.css('color', 'red');
            notie.alert_show(3, 'You are not allowed to buy less than 6000 CMD.');
        },

        hideErrorAmount: function () {
            this.tokenCountLabel.css('color', '#fff');
            notie.alert_hide();
        }
    };

    calculator.initialize();

    function getQrCodeImageUrl() {
        return 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' + selected_crypto_address;
    }

    function calculate_cmd(amount) {
        var converted_to_eth = amount * selected_rate;
        return converted_to_eth / cmd_rate;
    }

    function block_circle_buy_coin() {
        $('#blockui-element-container-default').block({
            message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6>Please, wait</h6></div>',
            overlayCSS: {
                background: 'rgb(255,255,255)',
                opacity: 1,
                cursor: 'wait'
            },
            css: {
                width: '50%'
            },
            blockMsgClass: 'block-msg-default'
        });
    }

    // Method for open ethereum modal
    function openEthereumModal() {
        $('.bd-example-modal-lg').modal('show');
    }

    function getExchangeRate(coin, callback) {
        var api_url = '';
        var rate = 0;
        if (coin && typeof callback == 'function') {
            switch (coin) {
                case 'eth':
                    rate = 1;
                    break;
                case 'teth':
                    rate = 1;
                    break;
                case 'btc':
                    api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=ETH';
                    break;
                case 'tbtc':
                    api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=ETH';
                    break;
                case 'tbch':
                    api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/?convert=ETH';
                    break;
                case 'bch':
                    api_url = 'https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/?convert=ETH';
                    break;
                case 'ltc':
                    api_url = 'https://api.coinmarketcap.com/v1/ticker/litecoin/?convert=ETH';
                    break;
                case 'tltc':
                    api_url = 'https://api.coinmarketcap.com/v1/ticker/litecoin/?convert=ETH';
                    break;
            }

            if (coin == 'eth' || coin == 'teth') {
                callback(rate);
            } else {
                $.getJSON(api_url, function (response) {
                    if (typeof response == 'string') {
                        response = JSON.parse(response);
                    }

                    var first_item = response[0];
                    if (first_item && first_item.price_eth) {
                        rate = first_item.price_eth;
                        callback(rate);
                    }
                });
            }
        }

        return false;
    }

    function initDetectTransactions(address) {
        var url = window.location.origin + '/get-transaction-status';
        var user_timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        if (user_timezone) {
            $.post(url, {
                address: address,
                datetime: open_page_datetime,
                time_zone: user_timezone,
                _token: window.Laravel.csrfToken
            }, function (response) {
                if (response.status) {
                    openSuccessModal(response.data.crypto_amount, response.data.cmd_count);
                    open_page_datetime = getDateTime();
                } else {
                    setTimeout(function () {
                        initDetectTransactions(address);
                    }, 10000);
                }
            });
        } else {
            console.log('cant get user timezone');
        }
    }

    function openSuccessModal(crypto_amount, cmd_count) {
        if (crypto_amount && cmd_count) {
            swal({
                title: "Transaction is successful!",
                text: "We received " + crypto_amount + " from you.\n" +
                cmd_count + " CMD are now added to your account.\n" +
                "You may now proceed to the main page to see them.",
                type: "success",
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"
            }, function () {
                window.location.href = window.location.origin;
            });
        }
    }

    function get_cmd_rate() {
        var url = window.location.origin + '/get-token-rate';
        $.getJSON(url, function (response) {
            if (response.rate) {
                console.log('cmd rate', response.rate);
                cmd_rate = parseFloat(response.rate);
            }
        });
    }
    get_cmd_rate();

    function show_error(msg) {
        $('#check_buy_coin').fadeOut(200);
    }

    function show_success(msg) {
        $('.error-alert-box').hide();
        $('.success-alert-box').text(msg);
        $('.success-alert-box').show();
    }

    function show_loader() {
        $.blockUI({
            overlayCSS:  {
                background: 'rgba(142, 159, 167, 0.3)',
                opacity: 1,
                cursor: 'wait'
            },
            css: {
                width: 'auto',
                top: '45%',
                left: '45%'
            },
            message: '<div class="blockui-default-message">Loading...</div>',
            blockMsgClass: 'block-msg-message-loader'
        });
        setTimeout($.unblockUI, 1000);
    }

    function hide_loader() {
        $('.loader').hide();
    }

    function getDateTime() {
        var date = new Date();
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min  = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec  = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day  = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
    }

    function getCoinLabel(coin) {
        var label = '';
        switch (coin) {
            case 'btc':
                label = 'BTC';
                break;
            case 'tbtc':
                label = 'BTC';
                break;
            case 'eth':
                label = 'ETH';
                break;
            case 'teth':
                label = 'ETH';
                break;
            case 'tbch':
                label = 'BCH';
                break;
            case 'bch':
                label = 'BCH';
                break;
            case 'ltc':
                label = 'LTC';
                break;
            case 'tltc':
                label = 'LTC';
                break;
        }

        return label;
    }
});