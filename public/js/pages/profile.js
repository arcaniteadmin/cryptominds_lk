$("#check_password").validate({
    rules: {
        password: {
            required: true,
            minlength: 6,
            equalTo: "#password_confirmation"
        },
        password_confirmation: {
            required: true,
            minlength: 6,
            equalTo: "#password"
        }
    },
    messages: {
        password: {
            required: "Please enter password",
            minlength: "Your password must be at least 6 characters long",
            equalTo: "Password does not match"
        },
        password_confirmation: {
            required: "Please enter password",
            minlength: "Your password must be at least 6 characters long",
            equalTo: "Password does not match"
        }
    },
    submitHandler: function(form) {
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function(response) {
                if (response.status == true) {
                    swal({
                            title: "Success!",
                            type: "success",
                            closeOnConfirm: false
                        },
                        function(){
                            window.location.replace('http://'+window.location.href.split('/')[2]);
                        });
                }
            }
        });
    }
});

$("#check_email").validate({
    rules: {
        email: {
            required: true,
        }
    },
    messages: {
        email: {
            required: "Please enter email",
        }
    },
    submitHandler: function(form) {
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function(response) {
                if (response.status == true) {
                    swal({
                            title: "Success!",
                            text: "You need to activate link that was sent to " + response.email,
                            type: "success",
                            closeOnConfirm: false
                        },
                        function(){
                            window.location.replace('http://'+window.location.href.split('/')[2]);
                        });
                }
            }
        });
    }
});

