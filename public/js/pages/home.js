
function updater(d, h, m, s) {

    var baseTime = new Date(2018, 6, 1, 12, 00);
    //var period = 3*24*60*60*1000;

    function update() {
        var cur = new Date();
        // сколько осталось миллисекунд
        var diff = baseTime - cur;
        if (diff <= 0) {
            d.innerHTML = 0;
            h.innerHTML = 0;
            m.innerHTML = 0;
            s.innerHTML = 0;
        } else {
            // сколько миллисекунд до конца секунды
            var millis = diff % 1000;
            diff = Math.floor(diff/1000);
            // сколько секунд до конца минуты
            var sec = diff % 60;
            if(sec < 10) sec = "0"+sec;
            diff = Math.floor(diff/60);
            // сколько минут до конца часа
            var min = diff % 60;
            if(min < 10) min = "0"+min;
            diff = Math.floor(diff/60);
            // сколько часов до конца дня
            var hours = diff % 24;
            if(hours < 10) hours = "0"+hours;
            var days = Math.floor(diff / 24);
            d.innerHTML = days;
            h.innerHTML = hours;
            m.innerHTML = min;
            s.innerHTML = sec;
        }

        // следующий раз вызываем себя, когда закончится текущая секунда
        setTimeout(update, millis);
    }
    setTimeout(update, 0);
}

updater(document.getElementById("days"),
    document.getElementById("hours"), document.getElementById("minutes"),
    document.getElementById("seconds"));

$(function() {
    $('.tab1').on('click', function() {
        $('.tab2').removeClass('active');
        $('#view1-tab-2').removeClass('active');
        $('.tab1').addClass('active');
        $('#view1-tab-1').addClass('active');

    });
    $('.tab2').on('click', function() {
        $('.tab1').removeClass('active');
        $('#view1-tab-1').removeClass('active');
        $('.tab2').addClass('active');
        $('#view1-tab-2').addClass('active');

    });

    function fadeIn_circle_progress_bar() {
        $('.block_view1_tab_1').removeClass('fade');
        $(".circle-progress-bar").asPieProgress({
            namespace: 'asPieProgress',
            speed: 500
        });
        $(".circle-progress-bar").asPieProgress("start");
        $(".circle-progress-bar-typical").asPieProgress({
            namespace: 'asPieProgress',
            speed: 25
        });
        $('.widget-chart-combo-content-in, .widget-chart-combo-side').matchHeight();
        $(".circle-progress-bar-typical").asPieProgress("start");
    }

    fadeIn_circle_progress_bar();

});


$('#eth_wallet_address').on( 'keyup change', function () {
    $('.form-group').removeClass('error');
    $('#wrong_adress').remove();
});
$('#eth_wallet_address').focus(function () {
    $('.form-group').removeClass('error');
    $('#wrong_adress').remove();

});
