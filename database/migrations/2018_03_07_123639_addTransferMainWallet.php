<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferMainWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_main_wallet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('coin');
            $table->string('wallet_id');
            $table->string('wallet_address');
            $table->string('main_wallet_address');
            $table->string('passphrase');
            $table->longText('json_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_main_wallet');
    }
}
