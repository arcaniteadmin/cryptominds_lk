<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTransactionDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('amount', 16, 8)->change();
            $table->decimal('rate', 16, 8)->change();
            $table->decimal('eth_amount', 16, 8)->change();
            $table->decimal('token_count', 16, 8)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('amount', 8, 2)->change();
            $table->decimal('rate', 8, 2)->change();
            $table->decimal('eth_amount', 8, 2)->change();
            $table->decimal('token_count', 8, 2)->change();
        });
    }
}
